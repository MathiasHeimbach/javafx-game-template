package de.awacademy;

import javafx.scene.media.*;
import java.nio.file.Paths;

public class Sound {

    private String bip1 = "src/voegelBach.mp3";
    private String bip2 = "src/yeahPunkt.mp3";
    private String bip3 = "src/gameover.mp3";

    MediaPlayer mediaPlayer;
    public void music(String bip){
        Media hit = new Media(Paths.get(bip).toUri().toString());
        mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();
    }

    public String getBip1() {
        return bip1;
    }

    public String getBip2() {
        return bip2;
    }

    public String getBip3() {
        return bip3;
    }
}
