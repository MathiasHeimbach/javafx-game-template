package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler (Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {

        /*if(keycode == KeyCode.UP) {
            model.getPlayer().move(0, -10);
        }
        if(keycode == KeyCode.DOWN) {
            model.getPlayer().move(0, 10);
        }*/
        if(keycode == KeyCode.LEFT) {
            if(model.getPlayer().getX() < 10 ){
                model.getPlayer().move(0, 0);
            }else{
            model.getPlayer().move(-10, 0);}
        }
        if(keycode == KeyCode.RIGHT) {
            if(model.getPlayer().getX() > 375 ){
                model.getPlayer().move(0, 0);
            }else{
            model.getPlayer().move(10, 0);}
        }
        if(keycode == KeyCode.ENTER && model.isGameover()) {
            model.getBall().setX((int) (Math.random() * 400));
            model.getBall().setY((int) (Math.random() * 500));
            model.setGameover(false);
            model.setScore(0);
        }
        if(keycode == KeyCode.SPACE && !model.isStart()) {
           model.setStart(true);
        }
    }
}
