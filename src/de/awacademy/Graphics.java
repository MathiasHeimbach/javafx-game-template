package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    private Image hintergrund;
    private Image playerbild;
    private Image ballbild;
    private String pointsText;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
        this.hintergrund = new Image("hintergrund.png");
        this.playerbild = new Image("playerbild.png");
        this.ballbild = new Image("ballbild.png");
    }

    public void draw() {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);


        gc.drawImage(hintergrund, 0, 0);
        gc.setFill(Color.YELLOW);
        gc.fillOval(model.getBall().getX(), model.getBall().getY(),
                model.getBall().getW(), model.getBall().getH()
        );

        gc.setFill(Color.RED);
        gc.fillRect(model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getW(), model.getPlayer().getH()
        );

        gc.drawImage(ballbild, model.getBall().getX(), model.getBall().getY(), model.getBall().getW(), model.getBall().getH());
        gc.drawImage(playerbild, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH()
        );

        gc.setFont(new Font("", 40));
        pointsText = "Score: " + model.getScore();
        gc.fillText(pointsText, 330, 36);
        gc.strokeText(pointsText, 330, 36);

        if (!model.isStart()) {
            gc.setFont(new Font("", 40));
            gc.fillText("FIRE DESERT", 150, 380);
            gc.strokeText("FIRE DESERT", 150, 380);
            gc.fillText("PRESS SPACE TO START", 30, 430);
            gc.strokeText("PRESS SPACE TO START", 30, 430);
        }

        if (model.isGameover()) {
            gc.setFont(new Font("", 40));
            gc.fillText("GAMEOVER", 150, 380);
            gc.strokeText("GAMEOVER", 150, 380);
            gc.fillText("PRESS ENTER TO RESTART", 30, 430);
            gc.strokeText("PRESS ENTER TO RESTART", 30, 430);
        }

    }

}
