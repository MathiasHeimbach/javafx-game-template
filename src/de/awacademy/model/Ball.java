package de.awacademy.model;

public class Ball {

    private int x;
    private int y;
    private int h;
    private int w;

    public Ball(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 50;
        this.w = 50;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setW(int w) {
        this.w = w;
    }
}


