package de.awacademy.model;

import de.awacademy.Sound;

import java.io.FileNotFoundException;

public class Model {

    private Player player;
    private Ball ball;
    private int score;
    private boolean gameover;
    private boolean start;
    private boolean level2 = true;

    public final double WIDTH = 500;
    public final double HEIGHT = 800;

    private Sound sound;

    public Model() {
        this.player = new Player(300, 760);
        this.ball = new Ball(280, 200);
    }

    Sound sound1 = new Sound();

    public void update(long nowNano) {
        touch();
        if (start) {
            if (score > 5 && level2) {
                ball.move(0, 5);
            } else {
                ball.move(0, 4);
            }
        } else {
            sound1.music(sound1.getBip1());
        }
    }

    public void touch() {
        if (Math.abs(player.getX() - ball.getX()) < 50 && Math.abs(player.getY() - ball.getY()) < 50) {
            score++;
            ball.setX((int) (Math.random() * 400));
            ball.setY((int) (Math.random() * 500));
            sound1.music(sound1.getBip2());
        } else if (ball.getY() > 800) {
            gameover = true;
            sound1.music(sound1.getBip3());
            if(level2){
                level2 = false;
            }
        }

    }

    public Player getPlayer() {
        return player;
    }

    public Ball getBall() {
        return ball;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isGameover() {
        return gameover;
    }

    public void setGameover(boolean gameover) {
        this.gameover = gameover;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean begin) {
        this.start = begin;
    }

    public boolean isLevel2() {
        return level2;
    }
}
